const { resolve } = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const urlLoader = require.resolve('./loaders/url-loader');
const fileLoader = require.resolve('./loaders/file-loader');
const cssOutputPath = 'style/test';

module.exports = (argv = {}) => ({
	entry: './folder1/folder2/script1.js',
	output: {
		path: resolve(__dirname, 'js'),
		filename: `server.js`,
	},
	module: {
		rules: [
			{
				test: /\.css$/,
				use: ExtractTextPlugin.extract({
					fallback: 'style-loader',
					use: [{
						loader: 'css-loader',
						query: {
							minimize: false,
						},
					}],
				}),
			},
			{
				loader: urlLoader,
				test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/i,
				exclude: /node_modules/,
				query: {
					limit: 7000,
					mimetype: 'application/font-woff2',
					cssOutputPath,
					useRelativePath: true,
					outputPath: 'fonts',
					name: '[name].[hash:7].[ext]',
				},
			},
			{
				test: /\.(jpe?g|png|gif|svg)(\?v=\d+\.\d+\.\d+)?$/,
				loader: fileLoader,
				options: {
					useRelativePath: true,
				},
			},
		],
	},
	plugins: [
		new ExtractTextPlugin({
			filename: `${cssOutputPath ? cssOutputPath + '/' : ''}server.css`,
			disable: !!argv.dev,
			allChunks: true,
		}),
	],
});
